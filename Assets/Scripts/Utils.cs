﻿using System;
using System.IO;
using UnityEngine;


public static class Utils
{
	public static Texture2D LoadImageToTexture(string filePath) {

		Texture2D tex = null;
		byte[] fileData;

		if (File.Exists(filePath))     {
			fileData = File.ReadAllBytes(filePath);
			tex = new Texture2D(2, 2);
			tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			tex.Apply();
		}
		return tex;
	}
}


