//-----------------------------------------------------------------------
// <copyright file="AreaLearningInGameController.cs" company="Google">
//
// Copyright 2016 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Tango;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Lean.Touch;

/// <summary>
/// AreaLearningGUIController is responsible for the main game interaction.
/// 
/// This class also takes care of loading / save persistent data(marker), and loop closure handling.
/// </summary>
public class AreaLearningInGameControllerPlay_v2 : MonoBehaviour, ITangoPose, ITangoEvent, ITangoDepth {

	public Text tekst;
	public GameObject QuizPanel;
	public AudioSource audio_manager;
	public GameObject CanvasPlay;

	public AudioClip[] audio_table;

	public Texture[] textureTemplates;
	public RawImage pickedTexture;
	public RawImage viewportTemplate;

	private static List<Question> unanswearedQuestion;
	private Question currentQuestion;
	private bool[] congrats_table;

	/// <summary>
	/// Prefabs of different colored markers.
	/// </summary>
	public GameObject[] m_markPrefabs;

	/// <summary>
	/// The point cloud object in the scene.
	/// </summary>
	public TangoPointCloud m_pointCloud;

	/// <summary>
	/// The canvas to place 2D game objects under.
	/// </summary>
	public Canvas m_canvas;

	/// <summary>
	/// The touch effect to place on taps.
	/// </summary>
	public RectTransform m_prefabTouchEffect;

	/// <summary>
	/// Saving progress UI text.
	/// </summary>
	public UnityEngine.UI.Text m_savingText;

	/// <summary>
	/// The Area Description currently loaded in the Tango Service.
	/// </summary>
	[HideInInspector]
	public AreaDescription m_curAreaDescription;

	#if UNITY_EDITOR
	/// <summary>
	/// Handles GUI text input in Editor where there is no device keyboard.
	/// If true, text input for naming new saved Area Description is displayed.
	/// </summary>
	private bool m_displayGuiTextInput;

	/// <summary>
	/// Handles GUI text input in Editor where there is no device keyboard.
	/// Contains text data for naming new saved Area Descriptions.
	/// </summary>
	private string m_guiTextInputContents;

	/// <summary>
	/// Handles GUI text input in Editor where there is no device keyboard.
	/// Indicates whether last text input was ended with confirmation or cancellation.
	/// </summary>
	private bool m_guiTextInputResult;
	#endif

	/// <summary>
	/// If set, then the depth camera is on and we are waiting for the next depth update.
	/// </summary>
	private bool m_findPlaneWaitingForDepth;

	/// <summary>
	/// A reference to TangoARPoseController instance.
	/// 
	/// In this class, we need TangoARPoseController reference to get the timestamp and pose when we place a marker.
	/// The timestamp and pose is used for later loop closure position correction. 
	/// </summary>
	private TangoPoseController m_poseController;

	/// <summary>
	/// List of markers placed in the scene.
	/// </summary>
	private List<GameObject> m_markerList = new List<GameObject>();

	/// <summary>
	/// Reference to the newly placed marker.
	/// </summary>
	private GameObject newMarkObject = null;

	/// <summary>
	/// Current marker type.
	/// </summary>
	private int m_currentMarkType = 0;

	/// <summary>
	/// If set, this is the selected marker.
	/// </summary>
	private ARMarkerMag m_selectedMarker;

	/// <summary>
	/// If set, this is the rectangle bounding the selected marker.
	/// </summary>
	private Rect m_selectedRect;

	/// <summary>
	/// If the interaction is initialized.
	/// 
	/// Note that the initialization is triggered by the relocalization event. We don't want user to place object before
	/// the device is relocalized.
	/// </summary>
	private bool m_initialized = false;

	/// <summary>
	/// A reference to TangoApplication instance.
	/// </summary>
	private TangoApplication m_tangoApplication;

	private Thread m_saveThread;

	/// <summary>
	/// Unity Start function.
	/// 
	/// We find and assign pose controller and tango application, and register this class to callback events.
	/// </summary>
	public void Start() {
		m_poseController = FindObjectOfType<TangoPoseController>();
		m_tangoApplication = FindObjectOfType<TangoApplication>();

		if (m_tangoApplication != null) {
			m_tangoApplication.Register(this);
		}

		congrats_table = new bool[m_markPrefabs.Length];
		for(int i=0; i<congrats_table.Length; i++) {
			congrats_table [i] = false;
		}
	}

	public void SetActiveCanvasPlay(){
		CanvasPlay.SetActive (true);
	}

	/// <summary>
	/// Unity Update function.
	/// 
	/// Mainly handle the touch event and place mark in place.
	/// </summary>
	public void Update() {
		if (m_saveThread != null && m_saveThread.ThreadState != ThreadState.Running) {
			// After saving an Area Description or mark data, we reload the scene to restart the game.
			_UpdateMarkersForLoopClosures();
			//_SaveMarkerToDisk();
			#pragma warning disable 618
			Application.LoadLevel(Application.loadedLevel);
			#pragma warning restore 618
		}

		if (Input.GetKey(KeyCode.Escape)) {
			//#pragma warning disable 618
			//Application.LoadLevel(Application.loadedLevel);
			//#pragma warning restore 618
			SceneManager.LoadScene(0);
		}

		if (!m_initialized) {
			return;
		}

		if (EventSystem.current.IsPointerOverGameObject(0) || GUIUtility.hotControl != 0) {
			return;
		}

		//tekst.text = "";
		//Glavna stvar pri touchanju
		if (Input.touchCount == 1) {
			Touch t = Input.GetTouch(0);
			Vector2 guiPosition = new Vector2(t.position.x, Screen.height - t.position.y);
			Camera cam = Camera.main;
			RaycastHit hitInfo;

			if (t.phase != TouchPhase.Began) {
				return;
			}

			if (m_selectedRect.Contains(guiPosition)) {
				// do nothing, the button will handle it
			}
			else if (Physics.Raycast(cam.ScreenPointToRay(t.position), out hitInfo)) {
				// Found a marker, select it (so long as it isn't disappearing)!
				GameObject tapped = hitInfo.collider.gameObject;
				tekst.text = m_currentMarkType.ToString();

				Debug.Log ("KLEN: this is current marker: m_currentMarkType: "
					+ m_currentMarkType + " mark.m_type: " +tapped.GetComponent<ARMarkerMag>().m_type 
					+ "mark.m_type % m_markPrefabs.Length:" + tapped.GetComponent<ARMarkerMag>().m_type  % (m_markPrefabs.Length/2));

				if (tapped && 
					(tapped.GetComponent<ARMarkerMag>().m_type % (m_markPrefabs.Length/2)) == m_currentMarkType &&
					congrats_table[m_currentMarkType] == false)  {
						//set texture from file texture.png
						m_selectedMarker = tapped.GetComponent<ARMarkerMag>();

						ReplaceStatue ();	
						StartCoroutine(CongratsTransition());

				}else if(tapped && tapped.GetComponent<ARMarkerMag>().m_type != m_currentMarkType) {
					//kviz napačnega
					SetCurrentQuestion();
					Constants.scannerRunning = false;
				}
			} else {
				// Place a new point at that location, clear selection
				m_selectedMarker = null;

				//tukaj bi predvajal zvok od klika v prazno

				// Because we may wait a small amount of time, this is a good place to play a small
				// animation so the user knows that their input was received.
				RectTransform touchEffectRectTransform = Instantiate(m_prefabTouchEffect) as RectTransform;
				touchEffectRectTransform.transform.SetParent(m_canvas.transform, false);
				Vector2 normalizedPosition = t.position;
				normalizedPosition.x /= Screen.width;
				normalizedPosition.y /= Screen.height;
				touchEffectRectTransform.anchorMin = touchEffectRectTransform.anchorMax = normalizedPosition;
			}
		}
	}

	public Text questionText;
	public void SetCurrentQuestion(){

		//audio_manager = m_selectedMarker.GetComponent<AudioSource> ();
		audio_manager.clip = audio_table[0];
		audio_manager.Play ();

		QuizPanel.SetActive(true);
	}

	IEnumerator CongratsTransition(){

		audio_manager.clip = audio_table[2];
		audio_manager.Play ();

		congrats_table [m_currentMarkType] = true;

		yield return new WaitForSeconds (1f);
	}
		
	public void SetTreasureID(int number){
		m_currentMarkType = number;
	}

	public void SetTemplateID(int number){
		pickedTexture.texture = textureTemplates[number];
		viewportTemplate.texture = textureTemplates[number];
	}
		
	/// <summary>
	/// Application onPause / onResume callback.
	/// </summary>
	/// <param name="pauseStatus"><c>true</c> if the application about to pause, otherwise <c>false</c>.</param>
	public void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus && m_initialized) {
			// When application is backgrounded, we reload the level because the Tango Service is disconected. All
			// learned area and placed marker should be discarded as they are not saved.
			#pragma warning disable 618
			Application.LoadLevel(Application.loadedLevel);
			#pragma warning restore 618
		}
	}

	//Delete and Move functions
	public void ReplaceStatue() {
		Constants.scannerRunning = false;

		Debug.Log ("KLEN ReplaceStatue: m_currentMarkAlpha: " + m_currentMarkAlpha);
		m_markerList.Remove (m_selectedMarker.gameObject);

		m_markPrefabs[m_selectedMarker.m_type-2].transform.localScale = m_currentMarkScale;

		Texture texture = Utils.LoadImageToTexture (Constants.newTexture);
		m_markPrefabs[m_selectedMarker.m_type-2].GetComponent<Renderer> ().material.SetTexture ("_MainTex", texture);
		m_markPrefabs[m_selectedMarker.m_type-2].GetComponent<Renderer> ().material.EnableKeyword ("_MainTex");

		GameObject temp = Instantiate(m_markPrefabs[m_selectedMarker.m_type-2],
			m_selectedMarker.transform.position,
			m_selectedMarker.transform.rotation) as GameObject;

		m_markerList.Add (temp);
		//m_markerList.Remove(marker);
		Destroy (m_selectedMarker.gameObject);
		m_selectedMarker = null;
	}

	/// <summary>
	/// Set the marker type.
	/// </summary>
	/// <param name="type">Marker type.</param>
	public void SetCurrentMarkType(int type) {
		if (type != m_currentMarkType) {
			m_currentMarkType = type;
		}
	}

	public void ShowToastMessage() {
		AndroidHelper.ShowAndroidToastMessage("Find and tap the glowing object.", AndroidHelper.ToastLength.LONG);
	}

	/// <summary>
	/// Save the game.
	/// 
	/// Save will trigger 3 things:
	/// 
	/// 1. Save the Area Description if the learning mode is on.
	/// 2. Bundle adjustment for all marker positions, please see _UpdateMarkersForLoopClosures() function header for 
	///     more details.
	/// 3. Save all markers to xml, save the Area Description if the learning mode is on.
	/// 4. Reload the scene.
	/// </summary>

	/*
    public void Save() {
        StartCoroutine(_DoSaveCurrentAreaDescription());
    }
    */

	/// <summary>
	/// This is called each time a Tango event happens.
	/// </summary>
	/// <param name="tangoEvent">Tango event.</param>
	public void OnTangoEventAvailableEventHandler(Tango.TangoEvent tangoEvent) {
		// We will not have the saving progress when the learning mode is off.
		if (!m_tangoApplication.m_areaDescriptionLearningMode) {
			return;
		}

		if (tangoEvent.type == TangoEnums.TangoEventType.TANGO_EVENT_AREA_LEARNING
			&& tangoEvent.event_key == "AreaDescriptionSaveProgress") {
				m_savingText.text = "Saving. " + (float.Parse(tangoEvent.event_value) * 100) + "%";
		}
	}

	/// <summary>
	/// OnTangoPoseAvailable event from Tango.
	/// 
	/// In this function, we only listen to the Start-Of-Service with respect to Area-Description frame pair. This pair
	/// indicates a relocalization or loop closure event happened, base on that, we either start the initialize the
	/// interaction or do a bundle adjustment for all marker position.
	/// </summary>
	/// <param name="poseData">Returned pose data from TangoService.</param>
	public void OnTangoPoseAvailable(Tango.TangoPoseData poseData) {
		// This frame pair's callback indicates that a loop closure or relocalization has happened. 
		//
		// When learning mode is on, this callback indicates the loop closure event. Loop closure will happen when the
		// system recognizes a pre-visited area, the loop closure operation will correct the previously saved pose 
		// to achieve more accurate result. (pose can be queried through GetPoseAtTime based on previously saved
		// timestamp).
		// Loop closure definition: https://en.wikipedia.org/wiki/Simultaneous_localization_and_mapping#Loop_closure
		//
		// When learning mode is off, and an Area Description is loaded, this callback indicates a
		// relocalization event. Relocalization is when the device finds out where it is with respect to the loaded
		// Area Description. In our case, when the device is relocalized, the markers will be loaded because we
		// know the relatvie device location to the markers.
		if (poseData.framePair.baseFrame == 
			TangoEnums.TangoCoordinateFrameType.TANGO_COORDINATE_FRAME_AREA_DESCRIPTION &&
			poseData.framePair.targetFrame ==
			TangoEnums.TangoCoordinateFrameType.TANGO_COORDINATE_FRAME_START_OF_SERVICE &&
			poseData.status_code == TangoEnums.TangoPoseStatusType.TANGO_POSE_VALID) {
			// When we get the first loop closure/ relocalization event, we initialized all the in-game interactions.
			if (!m_initialized) {
				m_initialized = true;
				if (m_curAreaDescription == null) {
					Debug.Log("AndroidInGameController.OnTangoPoseAvailable(): m_curAreaDescription is null");
					return;
				}
				_LoadMarkerFromDisk();
			}
		}
	}

	/// <summary>
	/// This is called each time new depth data is available.
	/// 
	/// On the Tango tablet, the depth callback occurs at 5 Hz.
	/// </summary>
	/// <param name="tangoDepth">Tango depth.</param>
	public void OnTangoDepthAvailable(TangoUnityDepth tangoDepth) {
		// Don't handle depth here because the PointCloud may not have been updated yet.  Just
		// tell the coroutine it can continue.
		m_findPlaneWaitingForDepth = false;
	}

	/// <summary>
	/// Correct all saved marks when loop closure happens.
	/// 
	/// When Tango Service is in learning mode, the drift will accumulate overtime, but when the system sees a
	/// preexisting area, it will do a operation to correct all previously saved poses
	/// (the pose you can query with GetPoseAtTime). This operation is called loop closure. When loop closure happens,
	/// we will need to re-query all previously saved marker position in order to achieve the best result.
	/// This function is doing the querying job based on timestamp.
	/// </summary>
	private void _UpdateMarkersForLoopClosures() {
		// Adjust mark's position each time we have a loop closure detected.
		foreach (GameObject obj in m_markerList) {
			ARMarkerMag tempMarker = obj.GetComponent<ARMarkerMag>();

			if (tempMarker.m_timestamp != -1.0f) {
				TangoCoordinateFramePair pair;
				TangoPoseData relocalizedPose = new TangoPoseData();

				pair.baseFrame = TangoEnums.TangoCoordinateFrameType.TANGO_COORDINATE_FRAME_AREA_DESCRIPTION;
				pair.targetFrame = TangoEnums.TangoCoordinateFrameType.TANGO_COORDINATE_FRAME_DEVICE;
				PoseProvider.GetPoseAtTime(relocalizedPose, tempMarker.m_timestamp, pair);

				Matrix4x4 uwTDevice = TangoSupport.UNITY_WORLD_T_START_SERVICE
					* relocalizedPose.ToMatrix4x4()
					* TangoSupport.DEVICE_T_UNITY_CAMERA;

				Matrix4x4 uwTMarker = uwTDevice * tempMarker.m_deviceTMarker;

				obj.transform.position = uwTMarker.GetColumn(3);
				obj.transform.rotation = Quaternion.LookRotation(uwTMarker.GetColumn(2), uwTMarker.GetColumn(1));
			}
		}
	}

	float m_currentMarkAlpha = 1.0f; //alpha of puzzle marker loaded from XML
	ARMarkerMag m_currentMarkObject = null;
	Vector3 m_currentMarkScale;
	/// <summary>
	/// Load marker list xml from application storage.
	/// </summary>
	private void _LoadMarkerFromDisk() {
		// Attempt to load the exsiting markers from storage.
		//string path = Application.persistentDataPath + "/" + m_curAreaDescription.m_uuid + ".xml";
		string path = Constants.xmlPath + "/" + m_curAreaDescription.m_uuid + ".xml";

		var serializer = new XmlSerializer(typeof(List<MarkerData>));
		var stream = new FileStream(path, FileMode.Open);

		List<MarkerData> xmlDataList = serializer.Deserialize(stream) as List<MarkerData>;

		if (xmlDataList == null) {
			Debug.Log("AndroidInGameController._LoadMarkerFromDisk(): xmlDataList is null");
			return;
		}

		m_markerList.Clear();
		foreach (MarkerData mark in xmlDataList) {
			//we are loading a transparent prefab
			mark.m_type += 2; 

			//prilagodi scale
			m_markPrefabs[mark.m_type].transform.localScale = mark.m_scale;

			//prepare alpha for animation
			m_markPrefabs [mark.m_type].GetComponent<Renderer> ().material.color = new Color (m_markPrefabs [mark.m_type].GetComponent<Renderer> ().material.color.r, 
																							m_markPrefabs [mark.m_type].GetComponent<Renderer> ().material.color.g,
																							m_markPrefabs [mark.m_type].GetComponent<Renderer> ().material.color.b,
																							0f);
																							//mark.m_alpha);

			// Instantiate all markers' gameobject.
			GameObject temp = Instantiate(m_markPrefabs[mark.m_type],
											mark.m_position,
											mark.m_orientation) as GameObject;

			if ((mark.m_type % (m_markPrefabs.Length/2)) == m_currentMarkType) {
				Debug.Log ("KLEN: this is current marker: m_currentMarkType: "
					+ m_currentMarkType + " mark.m_type: " + mark.m_type
					+ "mark.m_type % m_markPrefabs.Length:" + mark.m_type % (m_markPrefabs.Length/2));
				m_currentMarkAlpha = mark.m_alpha;
				m_currentMarkScale = mark.m_scale;
			}

			//tukaj aktiviraj animacijo blinkanja
			Animator anim = temp.GetComponent <Animator> ();
			anim.enabled = true;
			//anim.SetTrigger("Radar");

			m_markerList.Add(temp);
		}

	}

	/// <summary>
	/// Convert a 3D bounding box represented by a <c>Bounds</c> object into a 2D 
	/// rectangle represented by a <c>Rect</c> object.
	/// </summary>
	/// <returns>The 2D rectangle in Screen coordinates.</returns>
	/// <param name="cam">Camera to use.</param>
	/// <param name="bounds">3D bounding box.</param>
	private Rect _WorldBoundsToScreen(Camera cam, Bounds bounds) {
		Vector3 center = bounds.center;
		Vector3 extents = bounds.extents;
		Bounds screenBounds = new Bounds(cam.WorldToScreenPoint(center), Vector3.zero);

		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(+extents.x, +extents.y, +extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(+extents.x, +extents.y, -extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(+extents.x, -extents.y, +extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(+extents.x, -extents.y, -extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(-extents.x, +extents.y, +extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(-extents.x, +extents.y, -extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(-extents.x, -extents.y, +extents.z)));
		screenBounds.Encapsulate(cam.WorldToScreenPoint(center + new Vector3(-extents.x, -extents.y, -extents.z)));
		return Rect.MinMaxRect(screenBounds.min.x, screenBounds.min.y, screenBounds.max.x, screenBounds.max.y);
	}

	/// <summary>
	/// Wait for the next depth update, then find the plane at the touch position.
	/// </summary>
	/// <returns>Coroutine IEnumerator.</returns>
	/// <param name="touchPosition">Touch position to find a plane at.</param>
	private IEnumerator _WaitForDepthAndFindPlane(Vector2 touchPosition) {
		m_findPlaneWaitingForDepth = true;

		// Turn on the camera and wait for a single depth update.
		m_tangoApplication.SetDepthCameraRate(TangoEnums.TangoDepthCameraRate.MAXIMUM);
		while (m_findPlaneWaitingForDepth) {
			yield return null;
		}

		m_tangoApplication.SetDepthCameraRate(TangoEnums.TangoDepthCameraRate.DISABLED);

		// Find the plane.
		Camera cam = Camera.main;
		Vector3 planeCenter;
		Plane plane;
		if (!m_pointCloud.FindPlane(cam, touchPosition, out planeCenter, out plane)) {
			yield break;
		}

		// Ensure the location is always facing the camera.  This is like a LookRotation, but for the Y axis.
		Vector3 up = plane.normal;
		Vector3 forward;
		if (Vector3.Angle(plane.normal, cam.transform.forward) < 175) {
			Vector3 right = Vector3.Cross(up, cam.transform.forward).normalized;
			forward = Vector3.Cross(right, up).normalized;
		} else {
			// Normal is nearly parallel to camera look direction, the cross product would have too much
			// floating point error in it.
			forward = Vector3.Cross(up, cam.transform.right);
		}

		// Instantiate marker object.
		newMarkObject = Instantiate(m_markPrefabs[m_currentMarkType], planeCenter, Quaternion.Euler(new Vector3(-88, -25, 100))) as GameObject;
		m_markerList.Add(newMarkObject);

		m_selectedMarker = null;
	}

	/// <summary>
	/// Data container for marker.
	/// 
	/// Used for serializing/deserializing marker to xml.
	/// </summary>
	[System.Serializable]
	public class MarkerData {
		/// <summary>
		/// Marker's type.
		/// 
		/// Red, green or blue markers. In a real game scenario, this could be different game objects
		/// (e.g. banana, apple, watermelon, persimmons).
		/// </summary>
		[XmlElement("type")]
		public int m_type;

		/// <summary>
		/// Position of the this mark, with respect to the origin of the game world.
		/// </summary>
		[XmlElement("position")]
		public Vector3 m_position;

		/// <summary>
		/// Rotation of the this mark.
		/// </summary>
		[XmlElement("orientation")]
		public Quaternion m_orientation;

		/// <summary>
		/// Scale of the this mark.
		/// </summary>
		[XmlElement("scale")]
		public Vector3 m_scale;

		/// <summary>
		/// Alpha value of the this mark.
		/// </summary>
		[XmlElement("alpha")]
		public float m_alpha;

	}

	[System.Serializable]
	public class Question{

		public string question;
		public bool isTrue;
	}
}
