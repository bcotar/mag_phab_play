﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gallery : MonoBehaviour {

	public ToggleGroup StatueToggle;
	public ToggleGroup TextureToggle;
	public GameObject StatuesCanvas;
	public GameObject TexturesCanvas;
	public GameObject ShowCanvas;

	public GameObject[] texturesList;
	public GameObject[] statuesList;
	public Texture[] textures;

	public Text tekst;

	private Texture tekstura;
	private List<int> list = new List<int>();
	private int ListIndex = 0;
	private int ListLength = 0;

	public void CheckStatueToggle (){
		if (StatueToggle.AnyTogglesOn () == false) {
			AndroidHelper.ShowAndroidToastMessage ("Please choose a statue.");
			return;
		} else {
			StatuesCanvas.SetActive (false);
			//TexturesCanvas.SetActive (true);

			foreach (GameObject t in texturesList) {
				if (t.GetComponent<ObjectID> ().objectID != Constants.StatueIndex) {
					t.gameObject.SetActive (false);
				}
			}
				
			for (int i = 0; i < texturesList.Length; i++ ){
				if (texturesList[i].GetComponent<ObjectID> ().objectID == Constants.StatueIndex) {
					ListLength++;
					list.Add(i);
				}
			}

			ShowCanvas.SetActive (true);
		}
	}

	public void CheckTextureToggle (){
		if (TextureToggle.AnyTogglesOn () == false) {
			AndroidHelper.ShowAndroidToastMessage ("Please choose a texture.");
			return;
		} else {
			TexturesCanvas.SetActive (false);
			ShowCanvas.SetActive (true);
		}
	}

	public void SetStatueIndex(int index){
		Constants.StatueIndex = index;
	}

	public void ResetFilter(){
		foreach (GameObject t in texturesList) {
			t.gameObject.SetActive (true);
		}
	}

	public void SaveTexture(Texture tex){
		tekstura = tex;
	}

	public void ShowStatue(){			
		foreach (GameObject t in statuesList) {
			if(t.gameObject.GetComponent<ARMarkerMag>().m_type == Constants.StatueIndex){

				tekstura = textures[list[ListIndex]];
				t.gameObject.GetComponent<Renderer> ().material.SetTexture ("_MainTex", tekstura);
				t.gameObject.GetComponent<Renderer> ().material.EnableKeyword ("_MainTex");

				t.gameObject.SetActive (true);
			}
		}
	}

	public void NextTexture(){

		if (ListIndex < ListLength - 1){
			ListIndex++;

			foreach (GameObject t in statuesList) {
				if(t.gameObject.GetComponent<ARMarkerMag>().m_type == Constants.StatueIndex){
					tekstura = textures[list[ListIndex]];
					t.gameObject.GetComponent<Renderer> ().material.SetTexture ("_MainTex", tekstura);
					t.gameObject.GetComponent<Renderer> ().material.EnableKeyword ("_MainTex");
				}
			}
		}
	}

	public void PreviousTexture(){
		if (ListIndex > 0) {
			ListIndex--;

			foreach (GameObject t in statuesList) {
				if (t.gameObject.GetComponent<ARMarkerMag> ().m_type == Constants.StatueIndex) {
					tekstura = textures [list [ListIndex]];
					t.gameObject.GetComponent<Renderer> ().material.SetTexture ("_MainTex", tekstura);
					t.gameObject.GetComponent<Renderer> ().material.EnableKeyword ("_MainTex");
				}
			}
		}
	}
		
}
